package com.transandroid;

import org.json.JSONException;
import org.json.JSONObject;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class ChangeNameActivity extends Activity{

	@ViewInject(R.id.changename_name)
	EditText nickname;
	@ViewInject(R.id.changename_sbumit)
	Button submit_btn;
	
	protected String host,token;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_name);
		CustomActionBar.initCustomActionBar(getActionBar(),ChangeNameActivity.this,this.getResources().getString(R.string.user_center_changename));
		ViewUtils.inject(this);
		
		// 从全局对象中获取认证数据
        SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
        host = settings.getString("Host", "www.caozhi6655.site/onlinetransapp/trans_admin");
        token = settings.getString("token", "");

	}
	
	// 登录界面控件的控制
    private void setUIEnable(boolean b){
    	nickname.setEnabled(b);
        submit_btn.setEnabled(b);
    }
	
	//提交修改名称，调用接口
	@OnClick(R.id.changename_sbumit)
	public void submitClock(View view){
		if(nickname.getText().equals("")){
			Toast.makeText(ChangeNameActivity.this,R.string.not_null,Toast.LENGTH_SHORT).show();
		}else {
			String changenameapi = "http://" + host +"/AppInterface/AppUser/updateNickname?";
			RequestParams requestParams = new RequestParams();
			requestParams.addBodyParameter("token",token);
			requestParams.addBodyParameter("nickname", nickname.getText().toString());
//			Log.e("================", changenameapi);
//			Log.e("================", token);
			HttpUtils httpUtils = new HttpUtils();
			httpUtils.configDefaultHttpCacheExpiry(1000);
			httpUtils.send(HttpMethod.POST,
					changenameapi,
					requestParams,
					new RequestCallBack<String>(){

						@Override
						public void onStart() {
							setUIEnable(false);
						}
				
						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							try{
								JSONObject jsonObject = new JSONObject(arg0.result);
								//Log.e("===========",jsonObject.toString());
								if(jsonObject.getString("result").equals("1")){
									//TODO 修改成功后的跳转事件
									Toast.makeText(ChangeNameActivity.this,R.string.change_success,Toast.LENGTH_SHORT).show();
								}else {
									Toast.makeText(ChangeNameActivity.this,R.string.change_fail,Toast.LENGTH_SHORT).show();
								}
								setUIEnable(true);
							}catch(JSONException e){
								Toast.makeText(ChangeNameActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
								setUIEnable(true);
							}
						}
						
						@Override
						public void onFailure(HttpException arg0, String arg1) {
							Toast.makeText(ChangeNameActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
							setUIEnable(true);
						}

				
			});
		}
		
	}
	
}
