package com.transandroid;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class GetImageActivity extends Activity{

    private String title;

    @ViewInject(R.id.get_image_title)
    TextView title_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trans_apply_get_image);
        ViewUtils.inject(this);

        Intent getTitle = getIntent();
        title = getTitle.getStringExtra("title");

        CustomActionBar.initCustomActionBar(getActionBar(),GetImageActivity.this,title);
        title_view.setText(title);
    }
}
