package com.transandroid;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.conn.scheme.HostNameResolver;
import org.json.JSONException;
import org.json.JSONObject;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


public class ChangeSexActivity extends Activity{
	
	private MyAdapter list_item_adapter;
	private String host, token;
	
	//用户最终选择文本信息
	private String finaltext = "";

	@ViewInject(R.id.listview_sex)
	ListView listView;
	
	private List<String> items = new ArrayList<String>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_sex);
		ViewUtils.inject(this);
		CustomActionBar.initCustomActionBar(getActionBar(), ChangeSexActivity.this, this.getResources().getString(R.string.user_center_sex));
		
		// 从全局对象中获取认证数据
        SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
        host = settings.getString("Host", "www.caozhi6655.site/onlinetransapp/trans_admin");
        token = settings.getString("token", "");
		
		items.add(this.getResources().getString(R.string.man));
		items.add(this.getResources().getString(R.string.woman));
		
		//初始化适配器
		list_item_adapter = new MyAdapter(this, R.layout.list_item);
		//为适配器加载数据
		list_item_adapter.addAll(items);
		//加载listitem的适配器
		listView.setAdapter(list_item_adapter);
		
		//Item的点击事件
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{
                    for(int ctr=0;ctr < items.size();ctr++){
                        if(position == ctr){
                            listView.getChildAt(ctr).setBackgroundColor(Color.CYAN);
                        }else {
                            listView.getChildAt(ctr).setBackgroundColor(Color.rgb(153,153,153));
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
                finaltext = listView.getItemAtPosition(position).toString();
            }
        });
	}
	
	@OnClick(R.id.change_sex_confirm)
	public void confirmClick(View view){
		if(finaltext.equals("")){
			//用户未选择任何属性值
		}else {
			
			String mapi = "http://" + host + "/AppInterface/AppUser/updateSex?";
			
			RequestParams requestParams = new RequestParams();
			requestParams.addBodyParameter("token",token);
			if(finaltext.equals("男")){
				requestParams.addBodyParameter("sex","0");
			}else {
				requestParams.addBodyParameter("sex","1");
			}
			
			HttpUtils httpUtils = new HttpUtils();
			httpUtils.configDefaultHttpCacheExpiry(1000);
			httpUtils.send(HttpMethod.POST, 
					mapi,
					requestParams,
					new RequestCallBack<String>(){

						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							try{
								JSONObject jsonObject = new JSONObject(arg0.result);
								//Log.e("===========",jsonObject.toString());
								if(jsonObject.getString("result").equals("1")){
									//TODO 修改成功后的跳转事件
									Toast.makeText(ChangeSexActivity.this,R.string.change_success,Toast.LENGTH_SHORT).show();
								}else {
									Toast.makeText(ChangeSexActivity.this,R.string.change_fail,Toast.LENGTH_SHORT).show();
								}
							}catch(JSONException e){
								Toast.makeText(ChangeSexActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
							}
						}
						
						@Override
						public void onFailure(HttpException arg0, String arg1) {
							Toast.makeText(ChangeSexActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
						}
			});
		}
	}
	
	//自定义listview的数据适配器
	public class MyAdapter extends ArrayAdapter<String>{
		private int resource;
		private final LayoutInflater inflater;

		public MyAdapter(Context context, int resource)
		{
			super(context,resource);
			this.resource = resource;
			inflater = LayoutInflater.from(context);
		}

		//重写ArrayAdapter中的getview方法来适应当前数据的显示
		@Override
		public View getView(int position, View row, ViewGroup parent) {
			TextItemHolder holder = null;
			if(row == null){
				row = inflater.inflate(resource, parent,false);
				holder = new TextItemHolder();
				ViewUtils.inject(holder,row);
				row.setTag(holder);
			}else {
				holder = (TextItemHolder)row.getTag();
			}

			//根据得到的数据来绘制每一行
			final String sex = this.getItem(position);
			holder.sex_text.setText(sex);
			return row;
		}
	}
	
	  // 持有者模式 用于加速列表
    public class TextItemHolder {
        @ViewInject(R.id.tx)
        private TextView sex_text;
    }

}
