package com.transandroid;


import android.app.Activity;
import android.os.Bundle;
import com.lidroid.xutils.ViewUtils;

public class ChargeActivity extends Activity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_charge);
        //初始化标题栏
        CustomActionBar.initCustomActionBar(getActionBar(),ChargeActivity.this,this.getResources().getString(R.string.main_charge));
        ViewUtils.inject(this);
    }
}
