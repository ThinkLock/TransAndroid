package com.transandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class TransApplyActivity extends Activity{

    //用来表示用户所处状态
    //0注册   1上传  2审核   3测试   4完成
    private int userState;

    //加载页面控件
    @ViewInject(R.id.image_indicator1)
    ImageView image_indicator1;
    @ViewInject(R.id.image_indicator2)
    ImageView image_indicator2;
    @ViewInject(R.id.image_indicator3)
    ImageView image_indicator3;
    @ViewInject(R.id.image_indicator4)
    ImageView image_indicator4;
    @ViewInject(R.id.image_indicator5)
    ImageView image_indicator5;
    @ViewInject(R.id.trans_apply_bottom)
    LinearLayout trans_bottom_layout;


    private String language,card_type;
    private String from_flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trans_apply);
        ViewUtils.inject(this);
        CustomActionBar.initCustomActionBar(getActionBar(),TransApplyActivity.this,this.getResources().getString(R.string.trans_apply_title));

        //TODO 首先获取用户状态，然后对页面的绘制做出判断
        userState=0;
        //获取从选择界面传回的选择信息
        language= this.getResources().getString(R.string.trans_language_tai);
        card_type =  this.getResources().getString(R.string.trans_card_id);

        Intent intent = getIntent();
        from_flag = intent.getStringExtra("from");
        Log.e("----------------",from_flag);
        if(from_flag.equals("1")){
            //从选择语言种类跳转回来的
            language = intent.getStringExtra("language");
        }else if(from_flag.equals("2")){
            card_type = intent.getStringExtra("card_type");
        }

       try{
           InitImageIndicator();
           InitBottomView();
       } catch (Exception e){
           Log.e("-----------------",e.toString());
       }
    }

    //加载图片指示器
    private void InitImageIndicator(){
        //对加载图片资源进行初始化判断
        switch (userState){
            case 0:
                //用户处于注册阶段，加载注册界面
                image_indicator1.setImageResource(R.drawable.trans_apply_step1);
                image_indicator2.setImageResource(R.drawable.trans_apply_step2_norm);
                image_indicator3.setImageResource(R.drawable.trans_apply_step3_norm);
                image_indicator4.setImageResource(R.drawable.trans_apply_step4_norm);
                image_indicator5.setImageResource(R.drawable.trans_apply_step5_norm);
                break;
            case 1:
                //用户处于上传阶段，加载上传界面
                image_indicator1.setImageResource(R.drawable.trans_apply_step1);
                image_indicator2.setImageResource(R.drawable.trans_apply_step2_focus);
                image_indicator3.setImageResource(R.drawable.trans_apply_step3_norm);
                image_indicator4.setImageResource(R.drawable.trans_apply_step4_norm);
                image_indicator5.setImageResource(R.drawable.trans_apply_step5_norm);
                break;
            case 2:
                //用户处于审核阶段，加载审核界面
                image_indicator1.setImageResource(R.drawable.trans_apply_step1);
                image_indicator2.setImageResource(R.drawable.trans_apply_step2_focus);
                image_indicator3.setImageResource(R.drawable.trans_apply_step3_focus);
                image_indicator4.setImageResource(R.drawable.trans_apply_step4_norm);
                image_indicator5.setImageResource(R.drawable.trans_apply_step5_norm);
                break;
            case 3:
                //用户处于测试阶段，加载测试界面
                image_indicator1.setImageResource(R.drawable.trans_apply_step1);
                image_indicator2.setImageResource(R.drawable.trans_apply_step2_focus);
                image_indicator3.setImageResource(R.drawable.trans_apply_step3_focus);
                image_indicator4.setImageResource(R.drawable.trans_apply_step4_focus);
                image_indicator5.setImageResource(R.drawable.trans_apply_step5_norm);
                break;
            case 4:
                //用户处于完成阶段，加载完成界面
                image_indicator1.setImageResource(R.drawable.trans_apply_step1);
                image_indicator2.setImageResource(R.drawable.trans_apply_step2_focus);
                image_indicator3.setImageResource(R.drawable.trans_apply_step3_focus);
                image_indicator4.setImageResource(R.drawable.trans_apply_step4_focus);
                image_indicator5.setImageResource(R.drawable.trans_apply_step5_focus);
                break;
        }
    }

    //加载页面布局
    private void InitBottomView(){
        //然后对页面的绘制做出判断
        trans_bottom_layout.removeAllViews();
        switch (userState){
            case 0:
                //用户处于注册阶段，加载注册界面
                View step1_view = LayoutInflater.from(TransApplyActivity.this).inflate(R.layout.step1_view_item, null);
                trans_bottom_layout.addView(step1_view);

                //处理内部的点击事件
                //获取内部控件
                TextView choice_language_textview = (TextView)step1_view.findViewById(R.id.choice_language);
                TextView choice_card_type_textview = (TextView)step1_view.findViewById(R.id.choice_card_type);
                EditText input_name = (EditText)step1_view.findViewById(R.id.trans_apply_step1_name);
                EditText input_country = (EditText)step1_view.findViewById(R.id.trans_apply_step1_country);
                LinearLayout choice_language = (LinearLayout)step1_view.findViewById(R.id.trans_apply_step1_language);
                LinearLayout choice_card_type = (LinearLayout)step1_view.findViewById(R.id.trans_apply_step1_cardtype);
                Button to_next_btn = (Button)step1_view.findViewById(R.id.trans_apply_step1_next);

                choice_language_textview.setText(language);
                choice_card_type_textview.setText(card_type);

                //选择语言种类，点击事件
                choice_language.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(TransApplyActivity.this,ChoiceLanguageActivity.class));
                    }
                });
                //选证件类型
                choice_card_type.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(TransApplyActivity.this,ChoiceCardTypeActivity.class));
                    }
                });
                //点击下一步的事件
                to_next_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //TODO 将usersate加1后，中心初始化图片指示器和底部view
                        userState++;
                        InitBottomView();
                        InitImageIndicator();
                    }
                });


                break;
            case 1:
                //用户处于上传阶段，加载上传界面
                View step2_view = LayoutInflater.from(TransApplyActivity.this).inflate(R.layout.step2_view_item, null);
                trans_bottom_layout.addView(step2_view);

                //注册界面中的控件
                LinearLayout choice_avatar = (LinearLayout)step2_view.findViewById(R.id.trans_apply_step2_avatar);
                LinearLayout choice_card = (LinearLayout)step2_view.findViewById(R.id.trans_apply_step2_card);
                LinearLayout choice_language_card = (LinearLayout)step2_view.findViewById(R.id.trans_apply_step2_language);
                LinearLayout choice_user_and_card = (LinearLayout)step2_view.findViewById(R.id.trans_apply_step2_user_and_card);

                final Intent intent = new Intent();
                intent.setClass(TransApplyActivity.this,GetImageActivity.class);

                //个人头像
                choice_avatar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intent.putExtra("title",TransApplyActivity.this.getResources().getString(R.string.trans_user_avatar));
                        startActivity(intent);
                    }
                });
                //个人证件
                choice_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intent.putExtra("title",TransApplyActivity.this.getResources().getString(R.string.trans_user_card));
                        startActivity(intent);
                    }
                });

                //语言资格证
                choice_language_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intent.putExtra("title",TransApplyActivity.this.getResources().getString(R.string.trans_user_language_card));
                        startActivity(intent);
                    }
                });

                //个人手持证件
                choice_user_and_card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        intent.putExtra("title",TransApplyActivity.this.getResources().getString(R.string.trans_user_and_card));
                        startActivity(intent);
                    }
                });

                break;
            case 2:
                //用户处于审核阶段，加载审核界面

                break;
            case 3:
                //用户处于测试阶段，加载测试界面

                break;
            case 4:
                //用户处于完成阶段，加载完成界面

                break;
        }
    }
}
