package com.transandroid;

import java.util.HashMap;

//import cn.sharesdk.framework.ShareSDK;
//import cn.sharesdk.onekeyshare.OnekeyShare;
//import cn.sharesdk.sina.weibo.SinaWeibo;

import android.app.ActionBar;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class UserCenterActivity extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_center);
		ViewUtils.inject(this);
        setActionBarLayout(R.layout.title_main, this,this.getResources().getString(R.string.main_center));
	}
	
	//更改名称点击事件
	@OnClick(R.id.user_changename)
	public void changeNameClick(View view){
		startActivity(new Intent(UserCenterActivity.this,ChangeNameActivity.class));
	}
	//更改性别点击事件
	@OnClick(R.id.user_sex)
	public void changeSexClick(View view){
		startActivity(new Intent(UserCenterActivity.this,ChangeSexActivity.class));
	}
	//更改国家点击事件
	@OnClick(R.id.user_country)
	public void countryClick(View view){
		startActivity(new Intent(UserCenterActivity.this,ChangeCountryActivity.class));
	}
	//更改密码点击事件
	@OnClick(R.id.user_changepsw)
	public void changePswClick(View view){
		startActivity(new Intent(UserCenterActivity.this,ChangePswActivity.class));
	}
	
	//分享事件
	@OnClick(R.id.user_share)
	public void shareClick(View view){
//		ShareSDK.initSDK(this,"fef69760433c");
//		HashMap<String,Object> hashMap = new HashMap<String, Object>();
//		hashMap.put("Id","1");
//		hashMap.put("SortId","1");
//		hashMap.put("AppKey","568898243");
//		hashMap.put("AppSecret","38a4f8204cc784f81f9f0daaf31e02e3");
//		hashMap.put("RedirectUrl","http://www.sharesdk.cn");
//		hashMap.put("ShareByAppClient","true");
//		hashMap.put("Enable","true");
//		ShareSDK.setPlatformDevInfo(SinaWeibo.NAME,hashMap);
//
//		 OnekeyShare oks = new OnekeyShare();
//		 //关闭sso授权
//		 oks.disableSSOWhenAuthorize();
//
//		// 分享时Notification的图标和文字  2.5.9以后的版本不调用此方法
//		 //oks.setNotification(R.drawable.ic_launcher, getString(R.string.app_name));
//		 // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
//		 oks.setTitle(getString(R.string.app_name));
//		 // titleUrl是标题的网络链接，仅在人人网和QQ空间使用
//		 oks.setTitleUrl("http://sharesdk.cn");
//		 // text是分享文本，所有平台都需要这个字段
//		 oks.setText("我是分享文本");
//		 // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
//		 oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
//		 // url仅在微信（包括好友和朋友圈）中使用
//		 oks.setUrl("http://sharesdk.cn");
//		 // comment是我对这条分享的评论，仅在人人网和QQ空间使用
//		 oks.setComment("我是测试评论文本");
//		 // site是分享此内容的网站名称，仅在QQ空间使用
//		 oks.setSite(getString(R.string.app_name));
//		 // siteUrl是分享此内容的网站地址，仅在QQ空间使用
//		 oks.setSiteUrl("http://sharesdk.cn");
//
//		 oks.setSilent(false);
//
//		// 启动分享GUI
//		 oks.show(this);
	}
	
	//了解我们点击事件
	@OnClick(R.id.user_about)
	public void aboutUsClick(View view){
		startActivity(new Intent(UserCenterActivity.this,AboutUsActivity.class));
	}
	
	//意见反馈点击事件
	@OnClick(R.id.user_suggest)
	public void suggestClick(View view){
		startActivity(new Intent(UserCenterActivity.this,FeedBackActivity.class));
	}
	
	//退出当前账号
	@OnClick(R.id.logout)
	public void logOutClick(View view){
        //弹出对话框
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(this.getResources().getString(R.string.exit));
        builder.setMessage(this.getResources().getString(R.string.exit_content));
        builder.setPositiveButton(this.getResources().getString(R.string.confirm), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            	//TODO 注销逻辑

            }
        });
        builder.setNegativeButton(this.getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });

        builder.create().show();
	}


    public void setActionBarLayout(int layoutId, Context mContext,String title) {
        ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

            LayoutInflater inflator = (LayoutInflater) this
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflator.inflate(layoutId, new LinearLayout(mContext),
                    false);
            ActionBar.LayoutParams layout = new ActionBar.LayoutParams(
                    ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
            actionBar.setCustomView(v, layout);
            TextView mTextView = (TextView)actionBar.getCustomView().findViewById(R.id.title_text);
            mTextView.setText(title);

            //TODO 登录逻辑判断
            actionBar.getCustomView().findViewById(R.id.title_avatar)
                    .setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View arg0) {
                            startActivity(new Intent(UserCenterActivity.this,LoginActivity.class));
                        }
                    });

            //TODO 消息列表跳转
            actionBar.getCustomView().findViewById(R.id.title_msg)
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
        }
    }
}
