package com.transandroid;

import org.json.JSONException;
import org.json.JSONObject;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ChangePswActivity extends Activity{
	
	private String host,token;
	
	@ViewInject(R.id.changepsw_old)
	EditText old_psw;
	@ViewInject(R.id.changepsw_new)
	EditText new_psw;
	@ViewInject(R.id.changepsw_newc)
	EditText confirm_psw;
	@ViewInject(R.id.changepsw_sbumit)
	Button submit_btn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_psw);
		CustomActionBar.initCustomActionBar(getActionBar(), ChangePswActivity.this, this.getResources().getString(R.string.user_center_changepsw));
		ViewUtils.inject(this);
		// 从全局对象中获取认证数据
        SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
        host = settings.getString("Host", "www.caozhi6655.site/onlinetransapp/trans_admin");
        token = settings.getString("token", "");
	}
	
	// 登录界面控件的控制
    private void setUIEnable(boolean b){
    	old_psw.setEnabled(b);
    	new_psw.setEnabled(b);
    	confirm_psw.setEnabled(b);
        submit_btn.setEnabled(b);
    }
	
	
	//提交新密码，调用接口
	@OnClick(R.id.changepsw_sbumit)
	public void submitClick(View view){
		final String old_psw_str = old_psw.getText().toString();
		final String new_psw_str = new_psw.getText().toString();
		final String confirm_psw_str = confirm_psw.getText().toString();
		//逻辑判断
		if(old_psw_str.isEmpty()||new_psw_str.isEmpty()||confirm_psw_str.isEmpty()){
			//输入为空
			Toast.makeText(ChangePswActivity.this,R.string.not_null,Toast.LENGTH_SHORT).show();
		}else if(!new_psw_str.equals(confirm_psw_str)){
			//两次密码不同
			Toast.makeText(ChangePswActivity.this,R.string.not_equal,Toast.LENGTH_SHORT).show();
		}else {
			//开始调用修改密码接口
			Log.e("=============",old_psw_str + new_psw_str + confirm_psw_str);
			//组装API
			String mapi = "http://" + host + "/AppInterface/AppUser/changePwd?";
			RequestParams requestParams = new RequestParams();
			requestParams.addBodyParameter("token",token);
			requestParams.addBodyParameter("password1",old_psw_str);
			requestParams.addBodyParameter("password2",new_psw_str);
			requestParams.addBodyParameter("password3",confirm_psw_str);
			
			//TODO 接口存在问题
			HttpUtils httpUtils = new HttpUtils();
			httpUtils.configDefaultHttpCacheExpiry(1000);
			httpUtils.send(HttpMethod.POST,
					mapi,
					requestParams,
					new RequestCallBack<String>(){

						@Override
						public void onStart() {
							setUIEnable(false);
						}
				
						@Override
						public void onSuccess(ResponseInfo<String> arg0) {
							try{
								JSONObject jsonObject = new JSONObject(arg0.result);
								Log.e("===========",jsonObject.toString());
								if(jsonObject.getString("result").equals("1")){
									//TODO 修改成功后的跳转事件
									Toast.makeText(ChangePswActivity.this,R.string.change_success,Toast.LENGTH_SHORT).show();
								}else {
									Toast.makeText(ChangePswActivity.this,R.string.change_fail,Toast.LENGTH_SHORT).show();
								}
								setUIEnable(true);
							}catch(JSONException e){
								Toast.makeText(ChangePswActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
								setUIEnable(true);
							}
						}
						
						@Override
						public void onFailure(HttpException arg0, String arg1) {
							Toast.makeText(ChangePswActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
							setUIEnable(true);
						}

				
			});
		}
	}
}
