package com.transandroid;

import java.util.ArrayList;
import java.util.List;

import android.content.SharedPreferences;
import android.util.Log;
import com.gc.flashview.FlashView;
import com.gc.flashview.constants.EffectConstants;
import com.gc.flashview.listener.FlashViewListener;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.event.OnClick;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends Activity {

	private FlashView flashView;
	private List<String> imageUrls = new ArrayList<String>();
	private String host;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setActionBarLayout(R.layout.title_main, this,this.getResources().getString(R.string.main_title));

        // 从全局对象中获取认证数据
        SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
        host = settings.getString("Host", "www.caozhi6655.site/onlinetransapp/trans_admin");
		//注入
		ViewUtils.inject(this);

		flashView=(FlashView)findViewById(R.id.flashview);
       //组装API
        String getImgsApi = "http://" +host + "/AppInterface/Index/getAdverts";
        //获取服务器轮播图片
        HttpUtils httpUtils = new HttpUtils();
        httpUtils.send(HttpRequest.HttpMethod.POST,
                getImgsApi,
                new RequestCallBack<String>(){

                    @Override
                    public void onSuccess(ResponseInfo<String> responseInfo) {
                        try{
                            JSONObject jsonObject = new JSONObject(responseInfo.result);
                            //Log.e("===========",jsonObject.toString());
                            if(jsonObject.getString("result").equals("1")){
                                //获取图片的数组
                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                Log.e("----------",jsonArray.toString());
                                for(int i=0;i<jsonArray.length();i++){
                                    //将数据填充到要返回的result中
                                    JSONObject imageObject = jsonArray.getJSONObject(i);
                                    //Log.e("-------------",imageObject.getString("org_img_path"));
                                    imageUrls.add("http://" + host + "/" + imageObject.getString("small_img_path"));
                                    //Log.e("-------------", imageUrls.get(i));

                                    flashView.setImageUris(imageUrls);
                                    flashView.setEffect(EffectConstants.DEFAULT_EFFECT);
                                    flashView.setOnPageClickListener(new FlashViewListener() {
                                        @Override
                                        public void onClick(int position) {
                                            //TODO 添加图片的点击事件
                                            Toast.makeText(getApplicationContext(), "你的点击的是第" + (position + 1) + "张图片！", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            }else {
                                Toast.makeText(MainActivity.this,R.string.change_fail,Toast.LENGTH_SHORT).show();
                            }
                        }catch(JSONException e){
                            Toast.makeText(MainActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(MainActivity.this,R.string.network_error,Toast.LENGTH_SHORT).show();
                    }
                });
	}

    //跳转我是翻译界面
    @OnClick(R.id.main_translate_img)
    public void translaterClick(View view){
        //TODO　添加是否已经注册的接口判断
//        if(!register()){
//            //跳转到成为翻译人员的注册界面
//            startActivity(new Intent(MainActivity.this,ChargeActivity.class));
//        }else {
//            //跳转到翻译人员的个人中心界面
//            startActivity(new Intent(MainActivity.this,ChargeActivity.class));
//        }
        startActivity(new Intent(MainActivity.this,ToBeTranslaterActivity_temp.class));
    }

    //跳转充值界面
    @OnClick(R.id.main_charge_img)
    public void chargeClick(View view){
        startActivity(new Intent(MainActivity.this,ChargeActivity.class));
    }

    //跳转个人中心
	@OnClick(R.id.main_center_img)
	public void UserCenterClick(View view){
		Intent intent = new Intent(MainActivity.this,UserCenterActivity.class);
		startActivity(intent);
	}

    //我要翻译
    @OnClick(R.id.main_want_trans)
    public void transClock(View view){

    }


    //标题栏初始化
	public void setActionBarLayout(int layoutId, Context mContext,String title) {
		ActionBar actionBar = getActionBar();
		if (null != actionBar) {
			actionBar.setDisplayShowHomeEnabled(false);
			actionBar.setDisplayShowCustomEnabled(true);
			actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

			LayoutInflater inflator = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			View v = inflator.inflate(layoutId, new LinearLayout(mContext),
					false);
			ActionBar.LayoutParams layout = new ActionBar.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
			actionBar.setCustomView(v, layout);
			TextView mTextView = (TextView)actionBar.getCustomView().findViewById(R.id.title_text);
			mTextView.setText(title);

			//TODO 登录逻辑判断
			actionBar.getCustomView().findViewById(R.id.title_avatar)
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						startActivity(new Intent(MainActivity.this,LoginActivity.class));
					}
				});

            //TODO 消息列表跳转
            actionBar.getCustomView().findViewById(R.id.title_msg)
                    .setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
		}
	}
}
