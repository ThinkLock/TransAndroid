package com.transandroid;

import org.json.JSONException;
import org.json.JSONObject;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.transandroid.R.string;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends Activity{

	@ViewInject(R.id.editText1)
	EditText telephone;
	@ViewInject(R.id.editText2)
	EditText password;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_test);
		ViewUtils.inject(this);
	}
	
	@OnClick(R.id.button1)
	public void btnClick(View veiw){
		// 获取输入
        final String tel = telephone.getText().toString();
        final String psw = password.getText().toString();

        // 检测空白输入
        if (tel.isEmpty() || psw.isEmpty()){
            Toast.makeText(LoginActivity.this, "kong", Toast.LENGTH_SHORT).show();
            return;
        }

        SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
        String host = settings.getString("Host", "www.caozhi6655.site/onlinetransapp/trans_admin");


        // login api
        String authorizeApi = "http://" + host + "/AppInterface/AppUser/login?";

        Log.e("=============",tel + psw);
        
        // 请求的参数
        RequestParams requestParams = new RequestParams();
        requestParams.addBodyParameter("telephone", tel);
        requestParams.addBodyParameter("password", psw);

        // 完成HTTP请求
        HttpUtils http = new HttpUtils();
        http.configDefaultHttpCacheExpiry(1000);
        http.send(com.lidroid.xutils.http.client.HttpRequest.HttpMethod.POST,
                authorizeApi,
                requestParams,
                new RequestCallBack<String>(){

                    @Override
                    public void onStart() {
                        // 禁用控件
                       
                    }

                    @Override
                    public void onSuccess(ResponseInfo<String> responseInfo) {
                        try {
                            // 获取JSON对象
                            JSONObject jsonResult = new JSONObject(responseInfo.result);
                          
                            if (jsonResult.getString("result").equals("1") ){
                                
                            	// 成功
                            	String token = jsonResult.getJSONObject("data").getString("token");
                            	Log.e("================", jsonResult.toString());
                                SharedPreferences settings = getSharedPreferences("settings", MODE_PRIVATE);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("token", token);
                                editor.commit();
                            	

                               
                            }else{
                                
                                Toast.makeText(LoginActivity.this,"result no 1", Toast.LENGTH_SHORT).show();
                                Log.e("================", jsonResult.toString());

                            }
                        } catch (JSONException e) {
                            Toast.makeText(LoginActivity.this,"error1",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(HttpException e, String s) {
                        Toast.makeText(LoginActivity.this,"error2",Toast.LENGTH_SHORT).show();
                    }
                });
	}
}
