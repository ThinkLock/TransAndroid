package com.transandroid;

import android.app.ActionBar;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class CustomActionBar {

	public static boolean initCustomActionBar(ActionBar mActionBar,final Activity context,String string){
		if(mActionBar == null){
			return false;
		}
		mActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		mActionBar.setDisplayShowCustomEnabled(true);
		mActionBar.setCustomView(R.layout.title_no_msg);
		TextView titleTextView = (TextView)mActionBar.getCustomView().findViewById(R.id.title_nomsg_text);
		titleTextView.setText(string);
		mActionBar.getCustomView().findViewById(R.id.title_nomsg_back)
			.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					context.finish();
				}
			});
		return true;
	}
}
