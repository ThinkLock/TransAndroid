package com.transandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.event.OnClick;


public class ToBeTranslaterActivity_temp extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tobe_translater_temp);

        ViewUtils.inject(this);
        CustomActionBar.initCustomActionBar(getActionBar(),ToBeTranslaterActivity_temp.this,this.getResources().getString(R.string.main_trans));

    }

    @OnClick(R.id.to_next)
    public void nextClick(View view){
        Intent intent = new Intent();
        intent.putExtra("from","0");
        intent.setClass(ToBeTranslaterActivity_temp.this, TransApplyActivity.class);
        Log.e("----------------", "funnck");
        try{
            startActivity(intent);
        }catch (Exception e){
            Log.e("-----------------",e.toString());
        }
    }
}
